<div align="center">
    <h1>Swarm Token</h1>
    <p>The ERC20 token implementation for the Swarm BZZ token</p>
</div>

---

# Index

#### [Getting Set Up](#getting-set-up)

#### [Deployment Set Up](#deployment-set-up)
* [Notes on the `.env` variables](#notes-on-the-env-variables)

#### [Smart Contract Design](#smart-contract-design)
* [Inheritance](#inheritance)

---

# Getting Set Up

1. Installing packages
    This project uses yarn, but npm can be used in its place. 
    Inside a terminal in the root of the project, start by installing all the packages by running:
    ```yarn```
2. Once the packages are installed, compile the Smart Contracts by running:
    ```yarn build```
3. Once the contracts are compiled, in a news terminal window in the root directory, run the following to start a local Ganache instance. This will allow you to test the contracts locally. 
    ```yarn start```
4. In your original terminal, run the following to test the Smart Contracts.
    ```yarn test```
5. Once the tests are finished running (and non of them failed) you can run:
    ```yarn cover```
    To see the coverage of the tests. Please note that as the token uses OpenZeppelin implementations of the ERC20 standard, test coverage is only showing the coverage of the smart contracts inside the contracts folder, and not the test coverage of the imported OZ contracts. 

---

# Deployment Set Up

A deployment script has been made to deploy this token onto 3 networks:
1. Local (for local testing)
2. Rinkeby (for further testing)
3. Mainnet (for production deployment)

To deploy to any of these networks you will need to create and set up an `.env` file. 
Follow the steps below to successfully set up your `.env` for deployment:

1. Create a file named `.env` in the root directory of the folder
2. Copy everything out of the `.env.example` file into your `.env` file
    Your file should now look like this:
    ```
    # Deployment private keys:
        DEPLOYER_PRIVATE_KEY_LOCAL=
        DEPLOYER_PRIVATE_KEY_RINKEBY=
        DEPLOYER_PRIVATE_KEY_MAINNET=

    # PRC provider keys 
        INFURA_API_KEY_RINKEBY=
        INFURA_API_KEY_MAINNET=

    # Etherscan keys for verifying deployment on Rinkeby and Mainnet
        ETHERSCAN_API_KEY_RINKEBY=
        ETHERSCAN_API_KEY_MAINNET=

    # Parameters for Deployment
        TOKEN_NAME=""
        TOKEN_SYMBOL=""
        TOKEN_DECIMALS=
        # Please not that this number will be scaled by TOKEN_DECIMALS for you
        TOKEN_CAP=""

    # Set up for use
        MINTER_ROLE_MULTISIG_PUBLIC=
    ```
3. Fill in the needed keys, addresses and private keys needed for the network you wish to deploy to. 
4. Run the following script to deploy to the network of your choosing:
    * `yarn deploy:local`
    * `yarn deploy:rinkeby`
    * `yarn deploy:mainnet`
5. Once the script has run successfully you will be given the address of the token in a table.
    ```
    ┌─────────┬──────────┬──────────────────────────────────────────────┐
    │ (index) │ Contract │                   Address                    │
    ├─────────┼──────────┼──────────────────────────────────────────────┤
    │    0    │ 'Token'  │ '0x1456d1d7BC82A64e389541f754351D85AE02a271' │
    └─────────┴──────────┴──────────────────────────────────────────────┘
    Your deployment script finished successfully!
    ```

## Notes on the `.env` variables
There is specific formatting needed for each of the various parameters. To ensure the script runs as expected please ensure your variables conform to the following restrictions:
* **Private keys must start with a `0x`.** If formatted incorrectly the script will fail. 
* **Infura keys must not be given as strings** (i.e do not add quotation marks). If formatted incorrectly the script will fail.
* The **parameters for deployment may require quotation marks**. The parameters that do (`TOKEN_NAME`, `TOKEN_SYMBOL` and `TOKEN_CAP`) have quotation marks to ensure correct formatting. If formatted incorrectly the script may fail or behave unexpectedly. 
* Take extra care with the `TOKEN_CAP` as this must be the cap without the needed decimals. **The deployment script will scale the `TOKEN_CAP` by the given `TOKEN_DECIMALS`.** If you pass it in scaled the script may fail (JavaScript cannot hand large numbers) or the number will be scaled twice. This will cause unexpected behaviour and may result in always failing transactions well after deployment. 
* The `MINTER_ROLE_MULTISIG_PUBLIC` must be a different address to the deployer address. There is a check for this in the script, and if both addresses are the same the script will error and end.

---

# Smart Contract Design

The Token is designed to inherit the following implementations from the OpenZeppelin library (V2):
1. `ERC20`
    The ERC20 standard as implemented in the OpenZeppelin library.
2. `ERC20Detailed`
    This extension allows for the storing of a name, symbol and explicit decimal. 
3. `ERC20Capped`
    This extension allows for an explicit cap on the number of tokens that can ever be in existence simultaneously .
4. `ERC20Mintable`
    This extension allows users with the `Mintable` role to mint tokens freely. This is included to allow for future compatibility with a bonding curve.
5. `ERC20Burnable`
    This allows a user to burn tokens, as well as allows approved addresses to burn tokens on the users behalf. This again is included for future compatibility. 

## Inheritance 

The `Token` contract inherits the above contracts as follows:
```
contract Token is ERC20Detailed, ERC20Capped, ERC20Burnable {
```

As you can see, it seems as if all the contracts are not there. But when we dig a bit deeper, we can see all the contracts are in fact, present. 

### `ERC20Detailed`
Detailed imports the IERC20 interface:
```
contract ERC20Detailed is IERC20 {
```

### `ERC20Capped`
Capped is Mintable, meaning that by inheriting capped we are inheriting mintable. 
```
contract ERC20Capped is ERC20Mintable {
```

### `ERC20Burnable`
Burnable inherits from ERC20, so we fully implement the interface imported by Detailed, meaning the contract is not abstract and all ERC20 functionality is present. 
```
contract ERC20Burnable is Context, ERC20 {
```

### Inheritance Visuals

Below we can see the inheritance (simply) laid out in order. Here we are only looking at the first level of inheritance, i.e we are not looking at what the inherited contracts inherit. 

#### Simplified 
This simple diagram shows how all the required extensions are inherited. 

<img src="./imgs/token-inheritance.png">

#### Complete

For the full inheritance structure, please see the below diagram

<img src="./imgs/token-inheritance-full.png">