require("dotenv").config();
// Contract ABIs
const TokenABI = require("../build/Token.json");
// Tools
const etherlime = require("etherlime-lib");
const ethers = require("ethers");

const fetch = require("node-fetch");

const defaultConfigs = {
  gasLimit: 4700000,
  gasPrice: 25000000000,
};

const url = "https://www.etherchain.org/api/gasPriceOracle";
const backupUrl = "https://api.etherscan.io/api?module=gastracker&action=gasoracle";

const deploy = async (network, secret) => {
  var RPC = null;
  var deployer = null;

  if (network == "local") {
    // Overriding default config for local test net
    defaultConfigs.chainId = 1337;
    // Setting private key for this network
    secret = process.env.DEPLOYER_PRIVATE_KEY_LOCAL;
    // Setting the RPC
    RPC = "http://localhost:8545/";

    deployer = new etherlime.JSONRPCPrivateKeyDeployer(
      secret,
      RPC,
      defaultConfigs
    );

    console.log("Deploying to local network");
  } else if (network == "rinkeby") {
    // Overriding default config for rinkeby test net
    defaultConfigs.chainId = 4;
    // Setting private key for this network
    secret = process.env.DEPLOYER_PRIVATE_KEY_RINKEBY;
    // Setting up the etherscan API key for verification
	defaultConfigs.etherscanApiKey = process.env.ETHERSCAN_API_KEY_RINKEBY;
    // Setting the RPC
    RPC = `https:/rinkeby.infura.io/v3/${process.env.INFURA_API_KEY_RINKEBY}`;

    deployer = new etherlime.InfuraPrivateKeyDeployer(
      secret,
      network,
      process.env.INFURA_API_KEY_RINKEBY,
      defaultConfigs
    );

    console.log("Deploying to Rinkeby network");
  } else if (network == "mainnet") {
    // Overriding default config for mainnet
    defaultConfigs.chainId = 1;
    // Setting private key for this network
    secret = process.env.DEPLOYER_PRIVATE_KEY_MAINNET;
    // Setting up the etherscan API key for verification
	defaultConfigs.etherscanApiKey = process.env.ETHERSCAN_API_KEY_MAINNET;
	// Setting the gas price to fast
    defaultConfigs.gasPrice = _getGasPrice(url, backupUrl);
    // Setting the RPC
    RPC = `https://mainnet.infura.io/v3/${process.env.INFURA_API_KEY_MAINNET}`;

    deployer = new etherlime.InfuraPrivateKeyDeployer(
      secret,
      network,
      process.env.INFURA_API_KEY_MAINNET,
      defaultConfigs
    );

    console.log("Deploying to main network");
  } else {
	throw new Error("Invalid network");
  }

  const deploy = (...args) => deployer.deploy(...args);

  if (
    ethers.utils.getAddress(process.env.MINTER_ROLE_MULTISIG_PUBLIC) ==
    ethers.utils.getAddress(deployer.signer.address)
  ) {
	throw new Error("Deployer and multisig are the same.");
  } else {
    let tokenInstance;
    // If the network is not local the contract will be deployed and verified through etherscan
    if (network != "local") {
      tokenInstance = await deployer.deploy(
        TokenABI,
        false,
        process.env.TOKEN_NAME,
        process.env.TOKEN_SYMBOL,
        process.env.TOKEN_DECIMALS,
        ethers.utils.parseUnits(
          process.env.TOKEN_CAP,
          process.env.TOKEN_DECIMALS
        )
      );
    } else {
      // If the network is local the contract will just be deployed
      tokenInstance = await deployer.deploy(
        TokenABI,
        false,
        process.env.TOKEN_NAME,
        process.env.TOKEN_SYMBOL,
        process.env.TOKEN_DECIMALS,
        ethers.utils.parseUnits(
          process.env.TOKEN_CAP,
          process.env.TOKEN_DECIMALS
        )
      );
    }

    await tokenInstance.addMinter(process.env.MINTER_ROLE_MULTISIG_PUBLIC);

    let multiSigUserRoleStatus = await tokenInstance.isMinter(
      process.env.MINTER_ROLE_MULTISIG_PUBLIC
    );

    await tokenInstance.renounceMinter();

    let deployerStatus = await tokenInstance.isMinter(deployer.signer.address);

    if (!multiSigUserRoleStatus) {
      console.error("Fatal: Multisig not added as minter");
    } else if (deployerStatus) {
    }

    console.log(deployerStatus);

    var token = { Contract: "Token", Address: tokenInstance.contract.address };

    console.table([token]);
  }
};

const _getGasPrice = async (url, backupUrl) => {
  try {
    const response = await fetch(url);
    const json = await response.json();
    if (json.fast) {
      let price = json.fast;
      return price;
    } else {
      console.error("First URL failed (invalid response)\nTrying back up...");
    }
  } catch (error) {
    // Try backup API.
    try {
      const responseBackup = await fetch(backupUrl);
      const jsonBackup = await responseBackup.json();
      if (jsonBackup.result && jsonBackup.result.SafeGasPrice) {
        return jsonBackup.result.SafeGasPrice;
      } else {
        throw new Error("Etherscan API: bad json response");
      }
    } catch (errorBackup) {
      throw new Error("Error receiving Gas price - back up failed");
    }
  }
};

module.exports = {
  deploy,
};
