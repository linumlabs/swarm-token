const { 
    ethers,
    etherlime,
    BigNumber,
    Token,
    MCurve,
    erc20,
    tokenTests
} = require("./Settings.js");

describe("📈 Curve Tests", () => {
    // Users
    let insecureDeployer = accounts[0];
    let user = accounts[1];
    let admin_one = accounts[2];
    let admin_two = accounts[3];
    let admin_three = accounts[4];

    // Deployer instance
    let deployer;

    // Contract instances
    let tokenInstance;
    let mCurveInstance;

    beforeEach(async () => {
        deployer = new etherlime.EtherlimeGanacheDeployer(insecureDeployer.secretKey);

        tokenInstance = await deployer.deploy(
            Token,
            false,
            erc20.constructor_valid.name,
            erc20.constructor_valid.symbol,
            erc20.constructor_valid.decimals,
            erc20.constructor_valid.cap
        );

        mCurveInstance = await deployer.deploy(
            MCurve,
            false,
            tokenInstance.contract.address
        );

        await tokenInstance.from(insecureDeployer).addMinter(
            mCurveInstance.contract.address
        );
    });

    describe("Mint", () => {
        it("Can mint", async () => {
            let userBalance = await tokenInstance.balanceOf(user.signer.address);

            await mCurveInstance.from(user).mintTokens(tokenTests.mint.less_than_cap);

            let userBalanceAfter = await tokenInstance.balanceOf(user.signer.address);

            assert.equal(
                userBalance.toString(),
                0,
                "User has pre existing balance"
            );
            assert.equal(
                userBalanceAfter.toString(),
                tokenTests.mint.less_than_cap,
                "User has incorrect balance after mint"
            );
        });

        it("Can mint to cap", async () => {
            let userBalance = await tokenInstance.balanceOf(user.signer.address);

            await mCurveInstance.from(user).mintTokens(tokenTests.mint.exactly_cap);

            let userBalanceAfter = await tokenInstance.balanceOf(user.signer.address);

            assert.equal(
                userBalance.toString(),
                0,
                "User has pre existing balance"
            );
            assert.equal(
                userBalanceAfter.toString(),
                tokenTests.mint.exactly_cap,
                "User has incorrect balance after mint"
            );
        });

        it("🚫 Can't mint more than cap", async () => {
            let userBalance = await tokenInstance.balanceOf(user.signer.address);

            await assert.revertWith(
                mCurveInstance.from(user).mintTokens(tokenTests.mint.more_than_cap), 
                erc20.errors.cap_exceeded
            );

            let userBalanceAfter = await tokenInstance.balanceOf(user.signer.address);

            assert.equal(
                userBalance.toString(),
                0,
                "User has pre existing balance"
            );
            assert.equal(
                userBalanceAfter.toString(),
                0,
                "User has incorrect balance after mint"
            );
        });
    });

    describe("Burn", () => {
        it("Can burn", async () => {
            let userBalance = await tokenInstance.balanceOf(user.signer.address);

            await mCurveInstance.from(user).mintTokens(tokenTests.mint.less_than_cap);

            let userBalanceAfter = await tokenInstance.balanceOf(user.signer.address);

            assert.equal(
                userBalance.toString(),
                0,
                "User has pre existing balance"
            );
            assert.equal(
                userBalanceAfter.toString(),
                tokenTests.mint.less_than_cap,
                "User has incorrect balance after mint"
            );

            await tokenInstance.from(user).approve(
                mCurveInstance.contract.address,
                tokenTests.mint.less_than_cap
            );

            let spenderBalance = await tokenInstance.allowance(
                user.signer.address,
                mCurveInstance.contract.address
            );

            assert.equal(
                spenderBalance.toString(),
                tokenTests.mint.less_than_cap.toString(),
                "Spender balance incorrect"
            );

            await mCurveInstance.from(user).burnTokens(tokenTests.mint.less_than_cap);

            let userBalanceAfterBurn = await tokenInstance.balanceOf(user.signer.address);

            assert.equal(
                userBalanceAfterBurn.toString(),
                0,
                "User has balance after burn"
            );
        });
    });
});
