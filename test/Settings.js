const etherlime = require('etherlime-lib');
const ethers = require('ethers');
const BigNumber = require('bignumber.js');
let Token = require('../build/Token.json');
let MCurve = require('../build/Mock_dont_audit_curve.json');

var erc20 = {
    events: {
        transfer: "Transfer",
        approval: "Approval",
        minterAdded: "MinterAdded",
        minterRemoved: "MinterRemoved"
    },
    errors: {
        transfer_sender: "ERC20: transfer from the zero address",
        transfer_recipient: "ERC20: transfer to the zero address",
        mint_account: "ERC20: mint to the zero address",
        burn_account: "ERC20: burn from the zero address",
        approve_owner: "ERC20: approve from the zero address",
        approve_spender: "ERC20: approve to the zero address",
        minter_is_minter: "MinterRole: caller does not have the Minter role",
        minter_has_role: "Roles: account already has role",
        minter_not_minter: "Roles: account does not have role",
        minter_is_role: "Roles: account is the zero address",
        cap_zero: "ERC20Capped: cap is 0",
        cap_exceeded: "ERC20Capped: cap exceeded"
    },
    constructor_valid: {
        name: "Swarm Token",
        symbol: "BZZ",
        decimals: 18,
        cap: ethers.utils.parseUnits("1000000", 18)
    },
    constructor_invalid: {
        cap: 0
    }
};

var tokenTests = {
    mint: {
        less_than_cap: ethers.utils.parseUnits("10", 18),
        exactly_cap: ethers.utils.parseUnits("1000000", 18),
        more_than_cap: ethers.utils.parseUnits("1000001", 18)
    },
    invalid: {
        zero_address: "0x0000000000000000000000000000000000000000"
    }
};

module.exports = {
    ethers,
    etherlime,
    BigNumber,
    Token,
    MCurve,
    erc20,
    tokenTests
}
