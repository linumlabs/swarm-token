pragma solidity ^0.5.0;

import "./Token.sol";

/**
  * @notice This contract is to be ignored, and is only for testing purposes.
  *         NOT AUDITED.
  */
contract Mock_dont_audit_curve {
    Token private TokenInstance;

    constructor(address _token) public {
        TokenInstance = Token(_token);
    }

    function mintTokens(uint256 _amount) public {
        /**
            Here the user would be charged, but this is not relevant to the 
            mint and burn tests, so the user will not need to pay or be refunded
         */
        require(
            TokenInstance.mint(
                msg.sender,
                _amount
            ), 
            "Mint failed"
        );
    }

    function burnTokens(uint256 _amount) public {
        // Here we check that the user has approved the curve as a spender
        // We do this because burnFrom uses the allowance 
        require(
            TokenInstance.allowance(
                msg.sender,
                address(this)
            ) >= _amount,
            "Approve curve to spend tokens"
        );

        // Here we are burning the token from the user
        TokenInstance.burnFrom(
            msg.sender,
            _amount
        );
        /**
            Here the user would be refunded, but this is not relevant to the 
            mint and burn tests, so the user will not need to pay or be refunded
         */
    }
}